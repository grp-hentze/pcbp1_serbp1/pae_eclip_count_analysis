all:
	quarto render PCBP1_SERBP1.qmd --to html -P parallel:FALSE
  
parallel: 
	quarto render PCBP1_SERBP1.qmd -P parallel:TRUE

fresh:
	quarto render PCBP1_SERBP1.qmd --to html -P parallel:FALSE --cache-refresh
	
freshp:
	quarto render PCBP1_SERBP1.qmd -P parallel:TRUE --cache-refresh

clean:  
	rm -rf out tmp