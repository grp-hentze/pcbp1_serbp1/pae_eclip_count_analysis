import gzip

def do_filter_file(input_file, filter_file, output_file):
    # Open the filter file and store the values in a set
    filter_values = set()
    with gzip.open(filter_file, 'rt') as filter_f:
        for line in filter_f:
            columns = line.strip().split('\t')
            filter_values.add(columns[0])

    # Filter the input file based on the values in the set
    with gzip.open(input_file, 'rt') as input_f, gzip.open(output_file, 'wt') as output_f:
        for line in input_f:
            columns = line.strip().split('\t')
            if columns[0] in filter_values:
                output_f.write(line)

# Usage example
input_file = "./input/gencodeV42_flattenedwindows_width100_steps20.txt.gz" # Path to the input file
filter_file = './tmp/windowIDs.txt.gz'  # Path to the filter file
output_file = './tmp/annotation.txt.gz'  # Path to the output file

do_filter_file(input_file, filter_file, output_file)
