# PAE_eCLIP_count_analysis


When running on seneca

```
cd /g/hentze/projects/eCLIP/PCBP1_SERBP1_se_Batch_AB_pae/analysis 
module load git-lfs
module load R/4.2.2-foss-2022b
module load quarto
```

Clone with submodules (dependent repositories)

```
git clone  --recurse-submodules https://git.embl.de/grp-hentze/pcbp1_serbp1/pae_eclip_count_analysis.git
```

if submodule cloning does not work, do:

```
cd pae_eclip_count_analysis
git clone https://git.embl.de/grp-hentze/pcbp1_serbp1/pcbp1-serbp1_dewseq.git
```

Start building the analysis document
```
cd pae_eclip_count_analysis
make all
```
