FROM rocker/tidyverse:4.2.2

# set environment varibles

ENV PATH /opt/conda/bin:${PATH}
ENV LANG C.UTF-8
ENV SHELL /bin/bash

# install snakemake

RUN apt-get update
RUN apt-get install -y libssl-dev libxml2-dev curl \
    && rm -rf /var/lib/apt/lists/*

RUN /bin/bash -c "curl -L https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-Linux-\$(uname -m).sh > mambaforge.sh && \
    bash mambaforge.sh -b -p /opt/conda && \
    conda config --system --set channel_priority strict && \
    rm mambaforge.sh"

RUN /bin/bash -c "mamba create -q -y -c conda-forge -c bioconda -n snakemake snakemake snakemake-minimal --only-deps && \
    source activate snakemake && \
    mamba install -q -y -c conda-forge singularity && \
    conda clean --all -y"  
# && \
#    which python && \
#    pip install .[reports,messaging,google-cloud]"

RUN echo "source activate snakemake" > ~/.bashrc

ENV PATH /opt/conda/envs/snakemake/bin:${PATH}

# install r libaries

RUN R -e "install.packages(c('tidyverse', 'openxlsx'))"

# add current directory as workspace 

COPY . /workspace
WORKDIR /workspace

# run 

#CMD make all
#CMD snakemake 

# to stop container from running
ENTRYPOINT ["tail"]
CMD ["-f","/dev/null"]
